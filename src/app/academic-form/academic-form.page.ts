import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-academic-form',
  templateUrl: './academic-form.page.html',
  styleUrls: ['./academic-form.page.scss'],
})
export class AcademicFormPage implements OnInit {

  public academicForm: FormGroup;
  levels: any;
  semesters: any;
  isReady = false;

  themeCover = 'https://uploads.weconnect.com/5697a23db8b961a51b2320fc2fca628221462aa2/40sp2tvpfou2fbsxvsil9a5y0pl.JPG';

  academicData = {
    user_id: '',
    level: '',
    semester: ''
  };
  isUpdating: boolean;

  constructor(private modalCtrl: ModalController,
    private auth: AuthService, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private toastCtrl: ToastController,
    private formBuilder: FormBuilder, private router: Router) {
    this.getLevel();
  }

  ngOnInit() {
    this.academicForm = this.formBuilder.group({
      level: [null, Validators.required],
      semester: [null, Validators.required]
    });
  }

  buildData() {
    this.academicData.user_id = localStorage.getItem('userID');
    this.academicData.level = this.academicForm.value.level;
    this.academicData.semester = this.academicForm.value.semester;
  }

  updateAcademics() {
    this.isUpdating = true;
    this.buildData();
    console.log(this.academicData);
    this.auth.store('academic_details', this.academicData).subscribe(response => {
      // console.log(response);
      if (response !== null || response !== undefined) {
        setTimeout(() => {
          this.toastCtrl.create({
            message: response['message'],
            duration: 2000
          }).then(toastEl => {
            this.isUpdating = false;
            toastEl.present();
            this.router.navigateByUrl('dashboard');
          });
        }, 3000);
      }
    }, error => {
      this.isUpdating = false;
    });
  }

  getLevel() {
    this.loadingCtrl.create({
      message: 'Getting ready...'
    }).then(loadingEl => {
      loadingEl.present();
      return this.auth.get('level').subscribe(resData => {
        // console.log(resData['level']);
        if (!resData || resData['level'].length <= 0) {
          loadingEl.dismiss();
          console.log('No level info data available');
        }
        this.levels = resData['level'];
        this.getSemester();
        loadingEl.dismiss();
      }, error => {
        console.log(error);
      });
    });
  }

  getSemester() {
    this.loadingCtrl.create({
      message: 'Final setup...'
    }).then(loadingEl => {
      loadingEl.present();
      return this.auth.get('semester').subscribe(resData => {
        // console.log(resData['semester']);
        if (!resData || resData['semester'].length <= 0) {
          loadingEl.dismiss();
          console.log('Not available atm');
        }
        this.semesters = resData['semester'];
        this.isReady = true;
        loadingEl.dismiss();
      }, error => {
        console.log(error);
      });
    });
  }
}
