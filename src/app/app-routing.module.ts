import { AuthGuard } from './guard/auth.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  {
    path: 'tutorial',
    loadChildren: './tutorials/tutorial/tutorial.module#TutorialPageModule'
  },
  {
    path: 'signin',
    loadChildren: './signin/signin.module#SigninPageModule'
  },
  {
    path: 'signup',
    loadChildren: './signup/signup.module#SignupPageModule'
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'academic-form',
    loadChildren: './academic-form/academic-form.module#AcademicFormPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'personality-form',
    loadChildren: './personality-form/personality-form.module#PersonalityFormPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'forgot-password',
    loadChildren:
    './forgot-password/forgot-password.module#ForgotPasswordPageModule'
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
