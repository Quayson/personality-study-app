import { Router } from '@angular/router';
import { Component } from '@angular/core';

import {
  Platform,
  NavController,
  ToastController,
  AlertController,
  LoadingController
} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private navCtrl: NavController,
    private auth: AuthService,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  editProfile() {
    this.router.navigateByUrl('/edit-profile');
  }

  workStatus() {
    console.log('Work status changed.');
  }

  logout() {
    this.alertCtrl
      .create({
        header: 'Signing Out',
        message: 'Are you <strong>sure</strong> about logging out?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'Yes',
            handler: () => {
              this.signout();
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }

  signout() {
    this.loadingCtrl.create({
      message: 'Logging out...',
      spinner: 'bubbles',
      duration: 1500
    }).then(loadingEl => {
      loadingEl.present();
      localStorage.clear();
      this.router.navigateByUrl('/signin');
    });
  }
}
