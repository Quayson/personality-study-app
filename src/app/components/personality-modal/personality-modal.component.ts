import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-personality-modal',
  templateUrl: './personality-modal.component.html',
  styleUrls: ['./personality-modal.component.scss'],
})
export class PersonalityModalComponent implements OnInit {

  @Input() userPersonality: any;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}


  onCancel() {
    this.modalCtrl.dismiss();
  }
}
