import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-popmenu',
  templateUrl: './popmenu.component.html',
  styleUrls: ['./popmenu.component.scss'],
})
export class PopmenuComponent implements OnInit {

  openMenu = false;

  constructor(public navCtrl: NavController, private router: Router, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) { }

  ngOnInit() { }

  togglePopupMenu() {
    return this.openMenu = !this.openMenu;
  }


  openTest() {
    this.router.navigateByUrl('personality-form');
  }

  openAcademic() {
    this.router.navigateByUrl('academic-form');
  }

  openProfile() {
    this.router.navigateByUrl('');
  }




  logOut() {
    this.alertCtrl.create({
      header: 'Exit Application',
      message: 'Are you sure about closing appplication',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            this.signOut();
          }
        }
      ]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  signOut() {
    this.loadingCtrl.create({
      message: 'Logging out...',
      spinner: 'dots',
      duration: 3000
    }).then(loadingEl => {
      loadingEl.present();
      localStorage.removeItem('token');
      localStorage.removeItem('userID');
      this.router.navigateByUrl('signin');
    });
  }
}
