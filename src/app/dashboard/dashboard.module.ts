import { PersonalityModalComponent } from './../components/personality-modal/personality-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';

import { PopmenuComponent } from './../components/popmenu/popmenu.component';

import { NgCalendarModule  } from 'ionic2-calendar';



const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgCalendarModule
  ],
  declarations: [
    DashboardPage,
    PersonalityModalComponent,
    PopmenuComponent
  ],
  entryComponents: [PersonalityModalComponent]
})
export class DashboardPageModule { }
