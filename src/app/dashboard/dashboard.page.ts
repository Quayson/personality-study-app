import { Component, OnInit, ViewChild, Inject, LOCALE_ID } from '@angular/core';
import { LoadingController, AlertController, ToastController, ModalController, ActionSheetController } from '@ionic/angular';

import { AuthService } from './../services/auth.service';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';

import * as moment from 'moment';

import { PersonalityModalComponent } from '../components/personality-modal/personality-modal.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {

  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  userPersonality: any;
  semesterID: any;
  semesterData: any;
  calendarCourses: any;
  courses: any;

  event = {
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };


  minDate = new Date().toISOString();

  eventSource = [];

  viewTitle;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  constructor(private auth: AuthService, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private toastCtrl: ToastController,
    private modalCtrl: ModalController, private router: Router,
    @Inject(LOCALE_ID) private locale: string, private actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
    this.checkProfile();
  }

  ionViewDidEnter() {
    this.checkProfile();
  }

  openTest() {
    this.router.navigateByUrl('personality-form');
  }

  openAcademic() {
    this.router.navigateByUrl('academic-form');
  }


  logOut() {
    this.alertCtrl.create({
      header: 'Exit Application',
      message: 'Are you sure about closing appplication',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            this.signOut();
          }
        }
      ],
      backdropDismiss: false,
    }).then(alertEl => {
      alertEl.present();
    });
  }


  signOut() {
    this.loadingCtrl.create({
      message: 'Logging out...',
      spinner: 'dots',
      duration: 3000
    }).then(loadingEl => {
      loadingEl.present();
      localStorage.removeItem('token');
      localStorage.removeItem('userID');
      this.router.navigateByUrl('signin');
    });
  }
  onMenu() {
    this.actionSheetCtrl.create({
      header: 'Menu',
      buttons: [
        {
          text: 'Academics',
          icon: 'school',
          handler: () => {
            this.openAcademic();
          }
        },
        {
          text: 'Personality Test',
          icon: 'people',
          handler: () => {
            this.openTest();
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Actionsheet closed.');
          },
        },
        {
          text: 'Logout',
          icon: 'log-out',
          handler: () => {
            this.logOut();
          }

        }
      ]
    }).then(actionEl => {
      actionEl.present();
    });
  }

  resetEvent() {
    this.event = {
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }

  studyDetails(data) {
    this.modalCtrl.create({
      component: PersonalityModalComponent,
      componentProps: { userPersonality: data }
    }).then(modalEl => {
      modalEl.present();
    });
  }

  checkProfile() {
    this.loadingCtrl.create({
      message: 'Getting ready...'
    }).then(loadingEl => {
      loadingEl.present();
      return this.auth.show('academic_details', localStorage.getItem('userID')).subscribe(resData => {
        // console.log(resData['data']);
        if (resData['data'] === null || resData['data'] === undefined || resData['data'].length === 0) {
          loadingEl.dismiss();
          this.alertCtrl.create({
            header: 'Academic Details',
            message: 'Please provide academic details to complete your profile.',
            backdropDismiss: false,
            buttons: [
              {
                text: 'Okay',
                handler: () => {
                  this.router.navigateByUrl('academic-form');
                }
              }
            ]
          }).then(alertEl => {
            alertEl.present();
          });
        }
        loadingEl.dismiss();
        this.semesterData = resData['semester'];
        // console.log(this.semesterData);
        this.getPersonality();
      }, error => {
        console.log(error);
        loadingEl.dismiss();
      });
    });
  }


  getPersonality() {
    this.loadingCtrl.create({
      message: 'Getting Your Personality...'
    }).then(loadingEl => {
      loadingEl.present();
      return this.auth.show('user_personality', localStorage.getItem('userID')).subscribe(resData => {
        // console.log(resData['personality']);
        if (resData['personality'] === null || resData['personality'] === undefined || resData['personality'].length === 0) {
          loadingEl.dismiss();
          this.alertCtrl.create({
            header: 'User Personality',
            message: 'Please complete the personality test to continue.',
            backdropDismiss: false,
            buttons: [
              {
                text: 'Take Test',
                handler: () => {
                  this.router.navigateByUrl('personality-form');
                }
              }
            ]
          }).then(alertEl => {
            alertEl.present();
          });
        }
        loadingEl.dismiss();
        this.userPersonality = resData['personality'];
        this.getCourses();
      }, error => {
        console.log(error);
        loadingEl.dismiss();
      });
    });
  }

  getCourses() {
    this.loadingCtrl.create({
      message: 'Getting ready...'
    }).then(loadingEl => {
      loadingEl.present();
      return this.auth.show('user_courses', localStorage.getItem('userID')).subscribe(resData => {
        // console.log(resData);
        if (!resData || resData['data'] === null || resData['data'] === undefined || resData['data'].length === 0) {
          loadingEl.dismiss();
          this.alertCtrl.create({
            header: 'Academic Details',
            message: 'Please provide academic details to complete your profile.',
            backdropDismiss: false,
            buttons: [
              {
                text: 'Okay',
                handler: () => {
                  this.router.navigateByUrl('academic-form');
                }
              }
            ]
          }).then(alertEl => {
            alertEl.present();
          });
        }
        loadingEl.dismiss();
        this.calendarCourses = resData['data'];
        this.courses = Object.values(this.calendarCourses);
        this.addEvent(this.courses, this.semesterData);
      }, error => {
        console.log(error);
        loadingEl.dismiss();
      });
    });
  }

  // Calendar Functions
  // Create the right event format and reload source
  addEvent(courses, sem) {
    for (const key of courses) {
      for (let i = 0; i < courses.length; i++) {
        const eventCopy = {
          title: key['course_code'],
          startTime: new Date(sem['startDate']),
          endTime: new Date(sem['endDate']),
          allDay: false,
          desc: key['course_name']
        };

        if (eventCopy.allDay) {
          const start = eventCopy.startTime;
          const end = eventCopy.endTime;

          eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
          eventCopy.endTime = new Date(Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), end.getUTCDate() + 1));
        }

        this.eventSource.push(eventCopy);
        // console.log(this.eventSource);
        this.myCal.loadEvents();
      }
    }
  }

  // Change current month/week/day
  next() {
    const swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }

  back() {
    const swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }

  // Change between month/week/day
  changeMode(mode) {
    this.calendar.mode = mode;
  }

  // Focus today
  today() {
    this.calendar.currentDate = new Date();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  // Calendar event was clicked
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    const start = formatDate(event.startTime, 'medium', this.locale);
    const end = formatDate(event.endTime, 'medium', this.locale);

    const alert = await this.alertCtrl.create({
      subHeader: event.title,
      header: event.desc,
      message: 'From: ' + start + '<br><br>To: ' + end,
      buttons: ['OK']
    });
    alert.present();
  }

  // Time slot was clicked
  onTimeSelected(ev) {
    const selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());
  }

}

