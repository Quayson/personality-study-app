import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalityFormPage } from './personality-form.page';

describe('PersonalityFormPage', () => {
  let component: PersonalityFormPage;
  let fixture: ComponentFixture<PersonalityFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalityFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalityFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
