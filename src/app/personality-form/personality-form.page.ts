import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-personality-form',
  templateUrl: './personality-form.page.html',
  styleUrls: ['./personality-form.page.scss'],
})
export class PersonalityFormPage implements OnInit {

  constructor(private modalCtrl: ModalController,
    private auth: AuthService, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private toastCtrl: ToastController,
    private formBuilder: FormBuilder, private router: Router) { }

  public personalityForm: FormGroup;

  // questions = [];

  isChecking = false;

  personalityData = {
    user_id: localStorage.getItem('userID'),
    question_1: '',
    question_2: '',
    question_3: '',
    question_4: '',
    question_5: '',
    question_6: '',
    question_7: '',
    question_8: '',
    question_9: '',
    question_10: ''
  };


  ngOnInit() {
    this.personalityForm = this.formBuilder.group({
      question_1: [null, Validators.required],
      question_2: [null, Validators.required],
      question_3: [null, Validators.required],
      question_4: [null, Validators.required],
      question_5: [null, Validators.required],
      question_6: [null, Validators.required],
      question_7: [null, Validators.required],
      question_8: [null, Validators.required],
      question_9: [null, Validators.required],
      question_10: [null, Validators.required]
    });
  }

  buildData() {
    this.personalityData.question_1 = this.personalityForm.value.question_1;
    this.personalityData.question_2 = this.personalityForm.value.question_2;
    this.personalityData.question_3 = this.personalityForm.value.question_3;
    this.personalityData.question_4 = this.personalityForm.value.question_4;
    this.personalityData.question_5 = this.personalityForm.value.question_5;
    this.personalityData.question_6 = this.personalityForm.value.question_6;
    this.personalityData.question_7 = this.personalityForm.value.question_7;
    this.personalityData.question_8 = this.personalityForm.value.question_8;
    this.personalityData.question_9 = this.personalityForm.value.question_9;
    this.personalityData.question_10 = this.personalityForm.value.question_10;
  }


  checkPersonality() {
    this.isChecking = true;
    this.buildData();
   return this.auth.store('determine_personality', this.personalityData).subscribe(response => {
      // console.log(response);
      this.isChecking = false;
      if (response['data'] !== null || response['data'] !== undefined) {
        setTimeout(() => {
          this.toastCtrl.create({
            message: response['message'],
            duration: 2000
          }).then(toastEl => {
            this.isChecking = false;
            toastEl.present();
            this.router.navigateByUrl('dashboard');
          });
        }, 3000);
      }
    }, error => {
      this.isChecking = false;
      console.log(error);
    });
  }
}
