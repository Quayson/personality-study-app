import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';


const server = 'http://197.251.196.228:8000/api/';
// const server = 'http://127.0.0.1:8000/api/';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // setting token as variable
  token = localStorage.getItem('token');

  authState = new BehaviorSubject(false);
  contractorState = new BehaviorSubject(false);


  constructor(private http: HttpClient, private platform: Platform) {
    this.platform.ready().then(() => {
      this.isLoggedIn();
      this.isMember();
    });
  }


  isLoggedIn() {
    if (localStorage.getItem('token') || localStorage.getItem('token') !== null || localStorage.getItem('token') !== undefined) {
      this.authState.next(true);
    }
  }

  // Getting values to determine user type
  isMember() {
    if (localStorage.getItem('userType') === 'Contractor') {
      this.contractorState.next(true);
    }
  }

  // User authentication value
  isAuthenticated() {
    return this.authState.value;
  }

  // Contractor value
  isContractor() {
    return this.contractorState.value;
  }


  // User Data
  get userData() {
    return JSON.parse(localStorage.getItem('user'));
  }

  // index of a resource
  get(url) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.get(server + url, { headers: config });
  }


  // get info from store a new resource
  getInfo(url, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.post(server + url, payload, { headers: config });
  }

  // store a new resource
  store(url, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.post(server + url, payload, { headers: config });
  }

  // show a single resource
  show(url, id) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.get(server + url + '/' + id, { headers: config });
  }

  // show edit details for  a single resource
  edit(url, id) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.get(server + url + '/' + id, { headers: config });
  }

  // update a single resource
  update(url, id, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.patch(server + url + '/' + id, payload, {
      headers: config
    });
  }

  // delete a particular resource
  destroy(url, id) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.delete(server + url + '/' + id, { headers: config });
  }

  // registeration of users
  createUser(url, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    return this.http.post(server + url, payload, { headers: config });
  }

  // authentication for user login
  authenticate(payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    // return this.http.post('http://127.0.0.1:8000/oauth/token', payload, { headers: config });
    return this.http.post('http://197.251.196.228:8000/oauth/token', payload, { headers: config });
  }
}
