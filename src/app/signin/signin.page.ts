import { AuthService } from './../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  NavController,
  MenuController,
  ToastController,
  AlertController,
  LoadingController
} from '@ionic/angular';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss']
})
export class SigninPage implements OnInit {
  public loginForm: FormGroup;

  loginData = {
    username: '',
    password: '',
    grant_type: 'password',
    client_id: '2',
    client_secret: '5ANt87cP9qtgWMT4nGB27j7rIMgJ21GRhO2PnZrI',
    scope: '*'
  };

  isLoggin = false;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private route: Router,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(
            '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'
          )
        ])
      ],
      password: [
        null,
        Validators.compose([Validators.required, Validators.minLength(5)])
      ]
    });
  }

  buildData() {
    this.loginData.username = this.loginForm.value.email;
    this.loginData.password = this.loginForm.value.password;
  }

  signin() {
    this.isLoggin = true;
    this.buildData();
    this.auth.authenticate(this.loginData).subscribe(response => {
        // console.log(response);
        this.isLoggin = false;
          if (response !== null || response !== undefined) {
            localStorage.setItem('token', response['access_token']);
            this.checkAccessLevel(this.loginData);
            setTimeout(() => {
              this.isLoggin = false;
            }, 3000);
          }
      },
      error => {
        if (error.status === 404) {
          this.isLoggin = false;
          this.toastCtrl
            .create({
              message:
                'This credentials doesn`t match any user.',
              duration: 3000,
              showCloseButton: true
            })
            .then(toastEl => {
              toastEl.present();
            });
        } else if (error.status === 401) {
          this.isLoggin = false;
          this.toastCtrl
            .create({
              message: 'Your are not authorized to access this platform.',
              duration: 3000,
              showCloseButton: true
            })
            .then(toastEl => {
              toastEl.present();
            });
        } else {
          this.isLoggin = false;
          this.toastCtrl
            .create({
              message: 'Operation unsuccessful;, please try again later.',
              duration: 3000,
              showCloseButton: true
            })
            .then(toastEl => {
              toastEl.present();
            });
        }
      }
    );
  }


  checkAccessLevel(data) {
    this.auth.store('access_level', data).subscribe(response => {
      const access_level  = response['user']['status'];
      localStorage.setItem('user', JSON.stringify(response['user']));
      localStorage.setItem('userID', response['user']['id']);
      localStorage.setItem('username', response['user']['surname']);
      localStorage.setItem('userType', response['user']['status']);

      if (access_level === 'Student') {
        localStorage.setItem('access_level', 'Student');
        this.toastCtrl.create({
          message: 'Welcome ' + localStorage.getItem('username'),
          duration: 3000,
          position: 'top'
        }).then(toastEl => {
          toastEl.present();
        });

        this.navCtrl.navigateRoot(['dashboard']);
      }
    }, error => {
      console.log(error);
      this.alertCtrl.create({
        message: 'Service not available, please contact Admin asap!!!',
        buttons: ['Ok']
      }).then(toastEl => {
        toastEl.present();
      });
    });
  }
}
