import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialConPage } from './tutorial-con.page';

describe('TutorialConPage', () => {
  let component: TutorialConPage;
  let fixture: ComponentFixture<TutorialConPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialConPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialConPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
